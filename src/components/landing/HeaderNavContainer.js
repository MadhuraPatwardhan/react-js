import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { NavLink } from 'react-router-dom';
import Spinner from '../common/Spinner';


export const HeaderNavContainer = ({apiCallsInProgress}) => {
    return (
        <nav className="navbar navbar-toggleable-sm bg-info navbar-inverse">
            <div className="container">
                <button className="navbar-toggler" data-toggle="collapse" data-target="#mainNav">
                    <span className="navbar-toggler-icon" />
                </button>

                <div className="collapse navbar-collapse" id="mainNav">
                    <div className="navbar-nav">
                        <NavLink className="nav-item nav-link" exact activeClassName="active" to="/">NEWS</NavLink>
                        <NavLink className="nav-item nav-link" activeClassName="active" to="/videos" >VIDEOS</NavLink>
                        <NavLink className="nav-item nav-link" activeClassName="active" to="/pages">PAGES</NavLink>
                        <NavLink className="nav-item nav-link" exact activeClassName="active" to="/events">EVENTS</NavLink>
                        <NavLink className="nav-item nav-link" exact activeClassName="active" to="/advertise">ADVERTISE</NavLink>
                        <NavLink className="nav-item nav-link" exact activeClassName="active" to="/gallary">GALLARY</NavLink>
                        <NavLink className="nav-item nav-link" exact activeClassName="active" to="/contacts">CONTACTS</NavLink>
</div>
<div className="container">
<div className="text-right">
<a href="http://facebook.com" ><button className="fa fa-facebook" exact activeClassName="active"></button></a>
<a href="http://twitter.com" ><button className="fa fa-twitter" exact activeClassName="active"></button></a>
<a href="http://google-plus.com"><button className="fa fa-google-plus" exact activeClassName="active"></button></a>
<a href="http://linkedin.com" ><button className="fa fa-linkedin" exact activeClassName="active"></button></a>
<a href="http://pinterest.com"><button className="fa fa-pinterest" exact activeClassName="active"></button></a>
<a href="#"><button className="fa fa-search" exact activeClassName="active"></button></a>
</div>
</div>

<span className="ml-5">
    {apiCallsInProgress > 0 && <Spinner className="nav-item nav-link" interval={100} dots={20} />}
</span>


</div>
</div>

        </nav>
    );
};




HeaderNavContainer.propTypes = {
    apiCallsInProgress: PropTypes.number.isRequired
};



const mapStateToProps = state => ({
    apiCallsInProgress: state.apiReducer.apiCallsInProgress
});



export default connect(mapStateToProps)(HeaderNavContainer);
