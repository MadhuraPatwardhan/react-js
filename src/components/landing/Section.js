import React from 'react';
import reactjs from '../../img/reactjs.jpg';
import createReactApp from '../../img/createReactApp.jpg';
import reactrouter4 from '../../img/reactrouter4.jpg';
import bootstrap4 from '../../img/bootstrap4.jpg';
import centered from '../../style/style.css';
import data from '../data.json';

const Section = () => {
    return (


<div className="container-fluid">
        <section className="row">

            <div className="col-md-7">
                <div className="card">
                    <img src={reactjs} className="card-thumbnail" width="auto" height="500" alt="ReactJS"/>
                      <div className="centered">
                      <h3>{data.headline}</h3>
                      <p>ReactJS/Redux using the ES2015 (ES6) syntax.</p>
                    </div></div>
                </div>

                <div className="col-md-5">
                <div className="row">
                <div className="col-md-12">
                    <div className="card">
                        <img src={bootstrap4} className="card-thumbnail"  width="auto" height="265"alt="ReactJS"/>
                            <div className="centered1"> <h3>ReactJS</h3>
                            <p>ReactJS/Redux using the ES2015 (ES6) syntax.</p>
                        </div></div>
                        </div>
                        </div>

                        <div className="row">
                        <div className="col-md-12">
                            <div className="card">
                                <img src={reactrouter4} className="card-thumbnail" width="auto" height="235"alt="ReactJS"/>
                              <div className="centered1">       <h3>ReactJS</h3>
                                    <p>ReactJS/Redux using the ES2015 (ES6) syntax.</p>
                                </div>
                                </div>
                                </div>
                                </div>
    </div>
          </section>
          </div>

    );
};
export default Section;
